//
//  ViewController.swift
//  Google_Log_In
//
//  Created by Amol Tamboli on 29/04/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController, GIDSignInDelegate {

    @IBOutlet weak var lbl:UILabel!
    @IBOutlet weak var btn:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance()?.presentingViewController = self
        btn.addTarget(self, action: #selector(signinUserUsingGoogle(_:)), for: .touchUpInside)
    }
    
    @objc func signinUserUsingGoogle(_ sender:UIButton){
        
        if btn.title(for: .normal) == "Sign Out" {
            GIDSignIn.sharedInstance()?.signOut()
            lbl.text = ""
            btn.setTitle("Sign In Using Google", for: .normal)
        } else {
            GIDSignIn.sharedInstance()?.delegate = self
            GIDSignIn.sharedInstance()?.signIn()
        }
    }
    //MARK:- SignIn 
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("We have error sign in user == \(error.localizedDescription)")
        } else {
            if let gmailUser = user {
                lbl.text = "User Sign in \(gmailUser.profile.email!)"
                btn.setTitle("Sign Out", for: .normal)
            }
        }
    }

}

